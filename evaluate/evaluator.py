import pandas as pd
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt

class Evaluator:

    @staticmethod
    def compute_mse(y_pred, y_act):
        mse = mean_squared_error(y_pred, y_act)
        print(mse)

    @staticmethod
    def plot_predictions(y_pred, y_act):
        plt.plot(range(500), y_pred[0:500], color='green')
        plt.plot(range(500), y_act[0:500], color='blue')
        plt.show()

if __name__ == '__main__':
    pred_file = "meter_predictions.csv"
    pred_df = pd.read_csv(pred_file)
    pred = pred_df['pred']
    act = pred_df['meter_reading']

    # Evaluator.compute_mse(pred, act)
    Evaluator.plot_predictions(pred, act)

