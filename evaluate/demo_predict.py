
from preprocessing.building_processor import BuildingProcessor
from preprocessing.meter_processor import MeterProcessor
from utils.data_loader import DataLoader
from training.lightgbm_train import LightGBMTrain


building, weather_test, meter_test = DataLoader.load_test_data()
site_id = 1
buildings = BuildingProcessor.buildings_at_site(building, site_id)
building_id = buildings[0]
meters = BuildingProcessor.meters_in_building(meter_test, building_id)
meter_id = meters[0]
test_df  = MeterProcessor.prep_data_for_meter(building, meter_test, weather_test,
                                               site_id, building_id, meter_id, for_train=False)

features = test_df.drop(['row_id', 'building_id', 'meter', 'timestamp', 'site_id',
                        'primary_use', 'square_feet', 'year_built', 'floor_count'], axis = 1)

trainer = LightGBMTrain()
model_file = 'bgm_model.txt'
y_pred = trainer.predict(features, model_file)
print(y_pred)
