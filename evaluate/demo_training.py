
import numpy as np

from preprocessing.building_processor import BuildingProcessor
from preprocessing.meter_processor import MeterProcessor
from utils.data_loader import DataLoader
from training.lightgbm_train import LightGBMTrain

# prepare training data
building, weather_train, meter_train = DataLoader.load_train_data()
meter_train = meter_train[meter_train['meter_reading']!=0]
site_id = 1
buildings = BuildingProcessor.buildings_at_site(building, site_id)
building_id = buildings[0]
meters = BuildingProcessor.meters_in_building(meter_train, building_id)
meter_id = meters[0]
train_df  = MeterProcessor.prep_data_for_meter(building, meter_train, weather_train,
                                               site_id, building_id, meter_id)
target = np.log1p(train_df["meter_reading"])
features = train_df.drop(['building_id', 'meter', 'timestamp', 'meter_reading', 'site_id',
                          'primary_use', 'square_feet', 'year_built', 'floor_count'], axis = 1)

# run training
trainer = LightGBMTrain()
model_file = 'bgm_model.txt'
trainer.train(features, target, model_file=model_file)

# predict using training data
y_pred = trainer.predict(features, model_file)
pred_file = "meter_predictions.csv"
trainer.save_predictions(train_df, y_pred, pred_file)