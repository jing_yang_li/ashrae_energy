
import lightgbm as lgb
import gc
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error

from preprocessing.weather_processor import WeatherProcessor
from preprocessing.timeseries_processor import TimeseriesProcessor
from preprocessing.building_processor import BuildingProcessor
from preprocessing.meter_processor import MeterProcessor
from utils.data_loader import DataLoader


class LightGBMTrain:

    def train(self, features, target, model_file=None):
        # grid search for best params
        grid_scores, grid_params = self.grid_search(features, target)

        min_score = min(grid_scores)
        min_ind = grid_scores.index(min_score)
        best_params = grid_params[min_ind]

        # train with best params
        d_training = lgb.Dataset(features, label=target,free_raw_data=False)
        model = lgb.train(best_params, train_set=d_training, num_boost_round=1000,
                          valid_sets=[d_training], verbose_eval=25, early_stopping_rounds=50)
        y_pred = np.expm1(model.predict(features, num_iteration=model.best_iteration) )
        y_act  = np.expm1(target)
        score = mean_squared_error(y_pred, y_act)

        # save model to file
        if model_file is not None:
            model.save_model(model_file)
        print(score)

    def predict(self, features, model_file):
        # load the model
        model = lgb.Booster(model_file=model_file)

        y_pred = np.expm1(model.predict(features, num_iteration=model.best_iteration))
        return y_pred

    def grid_search(self, features, target):
        # g_num_leaves = [10, 30, 100, 300, 1000]
        g_num_leaves = [10, 30]
        # g_reg_lambda = [0.1, 0.3, 1, 3, 10, 30]
        g_reg_lambda = [0.1, 0.3]
        # g_learning_rate = [0.01, 0.03, 0.1, 0.3]
        g_learning_rate = [0.01, 0.03]
        # g_max_depth = [3,5,7]
        g_max_depth = [3]

        grid_scores = []
        grid_params = []
        for num_leaves in g_num_leaves:
            for reg_lambda in g_reg_lambda:
                for learning_rate in g_learning_rate:
                    for max_depth in g_max_depth:
                        params = {
                            "objective": "regression",
                            "boosting": "gbdt",
                            "num_leaves": num_leaves,
                            "learning_rate": learning_rate,
                            "feature_fraction": 0.85,
                            "reg_lambda": reg_lambda,
                            "metric": "rmse",
                            "max_depth": max_depth
                        }
                        score = self.cv_train(features, target, params)
                        grid_scores.append(score)
                        grid_params.append(params)

        return grid_scores, grid_params

    def cv_train(self, features, target, params):
        kf = KFold(n_splits=3)
        models = []
        scores = []
        for train_index,test_index in kf.split(features):
            train_features = features.loc[train_index]
            train_target = target.loc[train_index]

            test_features = features.loc[test_index]
            test_target = target.loc[test_index]

            #d_training = lgb.Dataset(train_features, label=train_target,categorical_feature=categorical_features, free_raw_data=False)
            #d_test = lgb.Dataset(test_features, label=test_target,categorical_feature=categorical_features, free_raw_data=False)
            d_training = lgb.Dataset(train_features, label=train_target,free_raw_data=False)
            d_test = lgb.Dataset(test_features, label=test_target,free_raw_data=False)

            model = lgb.train(params, train_set=d_training, num_boost_round=1000, valid_sets=[d_training,d_test], verbose_eval=25, early_stopping_rounds=50)

            #Prediction using test data
            y_pred = np.expm1(model.predict(test_features, num_iteration=model.best_iteration) )

            #Accuracy
            score = mean_squared_error(y_pred,np.expm1(test_target))
            scores.append(score)
            #print(model.eval_valid())
            models.append(model)
            del train_features, train_target, test_features, test_target, d_training, d_test
            gc.collect()
        print("Mean score: ", sum(scores)/len(scores))
        return sum(scores)/len(scores)

    def save_predictions(self, train_df, predictions, file_path, with_actuals=True):
        pred_df = pd.DataFrame(predictions, columns=['pred'])
        merge_df = pd.concat([train_df, pred_df], axis=1)
        merge_df.to_csv(file_path, columns=['timestamp', 'meter_reading', 'pred'],
                        index=False)


if __name__ == '__main__':
    # prepare training data
    building, weather_train, meter_train = DataLoader.load_train_data()
    meter_train = meter_train[meter_train['meter_reading']!=0]
    site_id = 1
    buildings = BuildingProcessor.buildings_at_site(building, site_id)
    building_id = buildings[0]
    meters = BuildingProcessor.meters_in_building(meter_train, building_id)
    meter_id = meters[0]
    train_df  = MeterProcessor.prep_data_for_meter(building, meter_train, weather_train,
                                                   site_id, building_id, meter_id)
    target = np.log1p(train_df["meter_reading"])
    features = train_df.drop(['building_id', 'meter', 'timestamp', 'meter_reading', 'site_id',
                          'primary_use', 'square_feet', 'year_built', 'floor_count'], axis = 1)

    # run training
    trainer = LightGBMTrain()
    model_file = 'bgm_model.txt'
    # trainer.train(features, target, model_file=model_file)

    # predict using training data
    y_pred = trainer.predict(features, model_file)
    pred_file = "meter_predictions.csv"
    trainer.save_predictions(train_df, y_pred)

