
from preprocessing.weather_processor import WeatherProcessor
from preprocessing.timeseries_processor import TimeseriesProcessor
from preprocessing.building_processor import BuildingProcessor
from utils.data_loader import DataLoader


class MeterProcessor:

    @staticmethod
    def preprocess_meter_reading(train, building_id, meter, for_train=True):

        # meter reading of meter in building_id
        df = train[(train['building_id']==building_id) & (train['meter']==meter)]

        # remove 0 values
        #df = df[df['meter_reading']!=0]

        # remove consecutive duplicates
        if for_train:
            df = df.loc[df.meter_reading.shift() != df.meter_reading]

        return df

    @staticmethod
    def prep_data_for_meter(building, meter, weather, site_id, building_id, meter_id, for_train=True):
        # Process weather data
        site_weather = weather[weather['site_id']==site_id]
        site_weather = WeatherProcessor.preprocess_weather(site_weather)

        # Process meter data
        building_meter = MeterProcessor.preprocess_meter_reading(meter, building_id, meter_id, for_train=for_train)

        # Merge building, meter reading (train), and weather data
        data_mg = TimeseriesProcessor.merge_data(building_meter, building, site_weather)

        TimeseriesProcessor.add_time_features(data_mg)

        return data_mg

if __name__ == '__main__':
    building, weather_train, meter_train = DataLoader.load_train_data()
    meter_train = meter_train[meter_train['meter_reading']!=0]
    site_id = 1
    buildings = BuildingProcessor.buildings_at_site(building, site_id)
    building_id = buildings[0]
    meters = BuildingProcessor.meters_in_building(meter_train, building_id)
    meter_id = meters[0]
    train_df  = MeterProcessor.prep_data_for_meter(building, meter_train, weather_train,
                                                   site_id, building_id, meter_id)

    test_df  = MeterProcessor.prep_data_for_meter(building, meter_train, weather_train,
                                                   site_id, building_id, meter_id, for_train=False)
    print(test_df.head())