
import pandas as pd

from pandas.tseries.holiday import USFederalHolidayCalendar as calendar


class TimeseriesProcessor:

    @staticmethod
    def add_time_features(df):
        df["hour"] = df["timestamp"].dt.hour
        df["weekday"] = df["timestamp"].dt.weekday
        df["is_weekend"] = (df["weekday"] > 5).astype(int)
        # convert to 1/0

        df.drop(['weekday'], axis=1, inplace=True)

        cal = calendar()
        holidays = cal.holidays(start=df["timestamp"].min(), end=df["timestamp"].max())
        df['is_holiday'] = (df['timestamp'].isin(holidays)).astype(int)

    @staticmethod
    def merge_data(meter_train, building, weather_train):
        train_mg = meter_train.merge(building, on='building_id', how='left')
        train_mg = train_mg.merge(weather_train, on=['site_id', 'timestamp'], how='left')
        train_mg['timestamp'] = pd.to_datetime(train_mg['timestamp'])

        return train_mg