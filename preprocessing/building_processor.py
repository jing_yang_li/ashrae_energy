
from utils.data_loader import DataLoader


class BuildingProcessor:

    @staticmethod
    def get_all_sites(building_df):
        return building_df['site_id'].unique()

    @staticmethod
    def buildings_at_site(building_df, site_id):
        df = building_df[building_df['site_id']==site_id]
        building_ids = df['building_id'].unique()
        return building_ids

    @staticmethod
    def meters_in_building(train_df, building_id):
        df = train_df[train_df['building_id']==building_id]
        meters = df['meter'].unique()
        return meters

if __name__ == '__main__':

    # buildings at a site
    building, weather_train, meter_train = DataLoader.load_train_data()
    sites = BuildingProcessor.get_all_sites(building)
    site_id = sites[1]
    building_ids = BuildingProcessor.buildings_at_site(building, site_id)
    print("site id =", site_id)
    print("building ids: ", building_ids)

    # meters in a building
    building_id = 106
    meters = BuildingProcessor.meters_in_building(meter_train, building_id)
    print("meter ids: ", meters)