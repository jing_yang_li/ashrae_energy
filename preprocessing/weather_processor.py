
import numpy as np
import pandas as pd

from utils.data_loader import DataLoader

class WeatherProcessor:

    @staticmethod
    def wind_spddir_to_uv(wspd, wdir):
        """
        Calculate the u and v wind components from wind speed and direction
        Input:
            wspd: wind speed
            wdir: wind direction
        Output:
            u: u wind component
            v: v wind component
        """
        if wspd is None or wdir is None:
            return None, None

        rad = 4.0*np.arctan(1)/180.
        u = -wspd*np.sin(rad*wdir)
        v = -wspd*np.cos(rad*wdir)

        return u,v

    @staticmethod
    def preprocess_weather(df):

        # drop cloud cover and precip
        # df.drop(columns=['precip_depth_1_hr','cloud_coverage'], inplace=True)
        df = df.drop(columns=['precip_depth_1_hr','cloud_coverage'])

        # convert wind spd and dir to U and V
        uv_df = pd.DataFrame(df[['wind_speed', 'wind_direction']].apply(lambda x:
                WeatherProcessor.wind_spddir_to_uv(x['wind_speed'],x['wind_direction']), axis=1).tolist())
        uv_df.columns = ['u', 'v']

        # sync the index
        # uv_df.index = df.index
        df['u'] = uv_df['u']
        # df.loc[:, 'u'] = uv_df['u']
        df['v'] = uv_df['v']
        # df.loc[:, 'v'] = uv_df['v']

        # drop wind speed and wind direction
        df.drop(columns=['wind_speed', 'wind_direction'], inplace=True)

        # fill missing weather data
        df.interpolate(method='linear', limit_direction='forward',inplace=True)

        return df


if __name__ == '__main__':

    site_id = 1
    # building, weather_train, meter_train = DataLoader.load_train_data()
    # site_weather = weather_train[weather_train['site_id']==site_id]
    # weather_df = WeatherProcessor.preprocess_weather(site_weather)
    # print(weather_df.head())

    a = [1,2,3,4]
    print(~0, ~1)