
import pandas as pd

class DataLoader:

    @staticmethod
    def load_train_data():
        building = pd.read_csv('/Users/jli/Documents/Projects/Kaggle/Ashrae/building_metadata.csv')
        weather_train = pd.read_csv('/Users/jli/Documents/Projects/Kaggle/Ashrae/weather_train.csv')
        meter_train = pd.read_csv('/Users/jli/Documents/Projects/Kaggle/Ashrae/train.csv')

        return building, weather_train, meter_train


    @staticmethod
    def load_test_data():
        building = pd.read_csv('/Users/jli/Documents/Projects/Kaggle/Ashrae/building_metadata.csv')
        weather_test = pd.read_csv('/Users/jli/Documents/Projects/Kaggle/Ashrae/weather_test.csv')
        meter_test = pd.read_csv('/Users/jli/Documents/Projects/Kaggle/Ashrae/test.csv')
        return building, weather_test, meter_test

    @staticmethod
    def merge_data(meter_train, building, weather_train):
        train_mg = meter_train.merge(building, on='building_id', how='left')
        train_mg = train_mg.merge(weather_train, on=['site_id', 'timestamp'], how='left')
        train_mg['timestamp'] = pd.to_datetime(train_mg['timestamp'])

        return train_mg

if __name__ == '__main__':
    building, weather_train, meter_train = DataLoader.load_train_data()